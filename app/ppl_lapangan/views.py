from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.db.models import Avg
from ..models import Lapangan, Jadwal, Review
from .forms import LapanganForm
from ..detail_review.forms import ReviewForm
from django.http import HttpResponse
from ..models import Lapangan, Booking
from ..booking.views import is_field_booked
import json

@login_required(login_url="/auth/login/")
def get_owned_lapangan(request):
    user = request.user
    if request.user.role == "pihak_penyedia_lapangan":
        lapangans = Lapangan.objects.filter(pemilik=user).order_by("nama")
    else:
        return HttpResponse("Anda tidak memiliki akses ke halaman ini", status=403)
    return render(request, "ppl_lapangan/owned_lapangan.html", {"lapangans": lapangans})


@login_required(login_url="/auth/login/")
def register_lapangan(request):
    if request.user.role == "pihak_penyedia_lapangan":
        if request.method == 'POST':
            form = LapanganForm(request.POST, request.FILES)
            if form.is_valid():
                lapangan = form.save(commit=False)
                lapangan.pemilik = request.user
                lapangan.no_telp = request.user.no_telp
                lapangan.save()
                form.save_m2m()
                return redirect('get_owned_lapangan') 
        else:
            form = LapanganForm()
        return render(request, 
            "ppl_lapangan/register_lapangan.html", {'form': form})
    else:
        return HttpResponse("Anda tidak memiliki akses ke halaman ini", status=403)
    

@login_required(login_url="/auth/login/")
def create_jadwal(request, lapangan_id):
    if request.method == 'POST':
        try:
            post_data = request.POST.copy()

            tanggal = post_data.pop('tanggal')[0]
            jam_mulai = post_data.pop('jam_mulai')[0]
            jam_selesai = post_data.pop('jam_selesai')[0]
            
            lapangan = get_object_or_404(Lapangan, pk=lapangan_id)
            lapangan.jadwals.create(tanggal=tanggal, jam_mulai=jam_mulai, jam_selesai=jam_selesai, status='tersedia')
            return redirect("lapangan_detail_owned", pk=lapangan_id)
        except:
            return HttpResponse("Server error, gagal menambahkan jadwal", status=400)
    else:
        return HttpResponse("Bad request, gagal menambahkan jadwal", status=400)


@login_required(login_url="/auth/login/")
def lapangan_detail(request, pk):
    lapangan = Lapangan.objects.filter(pk=pk)
    if lapangan.exists():
        lapangan = lapangan.get(pk=pk)
        lapangan.deskripsi = lapangan.deskripsi.strip()
        reviews = Review.objects.filter(lapangan__id=pk)

        if request.user.role == "pengguna":
            extracted_json = is_field_booked(request, pk).content.decode('utf-8')
            is_booked = json.loads(extracted_json).get('is_field_booked')
        else:
            is_booked = False
        
        no_description = all(review.deskripsi == "" for review in reviews)
        avg_rating = reviews.aggregate(Avg('rating'))['rating__avg'] or 0.0
        review_count = reviews.count()
        return render(request, 'detail_review/lapangan_detail.html', {'lapangan': lapangan, 
                                                                    'fromOwnedPage': True, 
                                                                    'reviews': reviews,
                                                                    'noDescription': no_description,
                                                                    'isBooked': is_booked,
                                                                    'avgRating': round(avg_rating, 2),
                                                                    'reviewCount': review_count,
                                                                    'reviewForm': ReviewForm})
    else:
        return redirect('home')

@login_required(login_url="/auth/login/")  
def view_history(request):
    if request.user.role == "pihak_penyedia_lapangan":
        try:
            user = request.user
            lapangans = Lapangan.objects.filter(pemilik=user).order_by("nama")
            lapangan_ids = [lapangan.id for lapangan in lapangans]
        except Lapangan.DoesNotExist:
            return HttpResponse("Lapangan tidak ditemukan", status=404)

        # Retrieve all bookings associated with the lapangan
        bookings = Booking.objects.filter(lapangan_id__in=lapangan_ids).order_by("waktu_pesan")

        return render(request, 
                      "ppl_lapangan/history_for_ppl.html", 
                      {'bookings': bookings})
    else:
        return HttpResponse("Anda tidak memiliki akses ke halaman ini", status=403)
    
@login_required(login_url="/auth/login/")
def update_lapangan(request, pk):
    if request.user.role == "pihak_penyedia_lapangan":
        try:
            lapangan = Lapangan.objects.get(pk=pk, pemilik=request.user)
        except Lapangan.DoesNotExist:
            return HttpResponse("Lapangan tidak ditemukan", status=404)

        if request.method == 'POST':
            form = LapanganForm(request.POST, request.FILES, instance=lapangan)
            if form.is_valid():
                updated_lapangan = form.save(commit=False)
                updated_lapangan.no_telp = request.user.no_telp
                updated_lapangan.save()
                form.save_m2m()
                return redirect('lapangan_detail_owned', pk=pk)
        else:
            form = LapanganForm(instance=lapangan, initial={
                'nama': lapangan.nama,
                'deskripsi': lapangan.deskripsi,
                'alamat': lapangan.alamat,
                'harga_perjam': lapangan.harga_perjam,
                'no_telp': lapangan.no_telp,
            })

        return render(request, 
                      "ppl_lapangan/update_lapangan.html", 
                      {'form': form, 'lapangan': lapangan})
    else:
        return HttpResponse("Anda tidak memiliki akses ke halaman ini", status=403)
