from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from ..models import Lapangan, Jadwal, Booking
from .forms import BookingForm
from datetime import datetime
from django.core import serializers


@login_required(login_url="/auth/login/")
def get_history(request):
    user = request.user
    if request.user.role == "pengguna":
        bookings = Booking.objects.filter(user=user).order_by("waktu_pesan")
    else:
        return HttpResponse("Anda tidak memiliki akses ke halaman ini", status=403)
    return render(request, "booking/booking_history.html", {"bookings": bookings})


@login_required(login_url="/auth/login/")
def add_booking(request, lapangan_id):
    if request.user.role == "pengguna":
        if request.method == "POST":
            user = request.user
            lapangan = Lapangan.objects.get(id=lapangan_id)
            form = BookingForm(lapangan_id, request.POST, request.FILES)
            if form.is_valid():
                selected_jadwals = form.cleaned_data.get("jadwals")
                booking = form.save(commit=False)
                booking.lapangan = lapangan
                booking.user = user

                booking.jumlah_jam = (
                    sum(
                        [
                            (
                                datetime.combine(datetime.min, j.jam_selesai)
                                - datetime.combine(datetime.min, j.jam_mulai)
                            ).total_seconds()
                            for j in selected_jadwals
                        ]
                    )
                    / 3600
                )

                booking.total_harga = booking.jumlah_jam * lapangan.harga_perjam
                booking.status = "menunggu"
                booking.save()
                booking.jadwals.set(selected_jadwals)

                for jadwal in selected_jadwals:
                    jadwal.status = "dipesan"
                    jadwal.save()

                return redirect("get_history")
        elif request.method == "GET":
            lapangan = Lapangan.objects.get(id=lapangan_id)
            form = BookingForm(lapangan_id=lapangan_id)

        return render(
            request,
            "booking/add_booking.html",
            {
                "lapangan": lapangan,
                "form": form,
            },
        )
    else:
        return HttpResponse("Anda tidak memiliki akses ke halaman ini", status=403)


@login_required(login_url="/auth/login/")
def booking_list(request):
    if request.user.role == "pihak_penyedia_lapangan":
        if request.method == "POST":
            # Handle accept or reject actions
            booking_id = request.POST.get("booking_id")
            action = request.POST.get("action")

            if action == "accept":
                booking = Booking.objects.get(id=booking_id)
                booking.status = "terverifikasi"
                booking.save()
                # Add any additional logic you need, such as sending notifications or updating availability.

            elif action == "reject":
                booking = Booking.objects.get(id=booking_id)
                booking.status = "gagal"
                booking.save()
                # Add any additional logic you need, such as sending notifications or updating availability.

            # Redirect back to the booking list
            return redirect("booking_list")

        else:
            user_lapangan_ids = Lapangan.objects.filter(pemilik=request.user).values_list('id', flat=True)
            bookings = Booking.objects.filter(lapangan__id__in=user_lapangan_ids)
            return render(
                request,
                "booking/booking_list.html",
                {"bookings": bookings},
            )

    else:
        return HttpResponse("Anda tidak memiliki akses ke halaman ini", status=403)


@login_required(login_url="/auth/login/")
def is_field_booked(request, lapangan_id):
    user = request.user
    if request.user.role == "pengguna":
        bookings = Booking.objects.filter(
            user=user, lapangan_id=lapangan_id, status="terverifikasi"
        ).all()
    else:
        return HttpResponse("Anda tidak memiliki akses ke halaman ini", status=403)
    # return response in json format
    return JsonResponse(
        {
            "is_field_booked": len(bookings) > 0,
        },
        status=200,
    )
