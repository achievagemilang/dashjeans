from django.urls import path, include
from .views import get_history, add_booking,booking_list, is_field_booked

urlpatterns = [
    path("", get_history, name="get_history"),
    path("add/<int:lapangan_id>/", add_booking, name="add_booking"),
    path('booking-list/', booking_list, name='booking_list'),
    path('isbooked/<int:lapangan_id>/', is_field_booked, name='is_field_booked'),
]
