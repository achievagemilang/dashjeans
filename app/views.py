from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.contrib.auth.decorators import login_required
import os

from app.models import Lapangan

def index(request):
    lapangans = Lapangan.objects.all()
    return render(request, "index.html", {"lapangans": lapangans})

@login_required
def bukti_pembayaran_page(request, file_name):
    file_path = os.path.join(settings.MEDIA_ROOT, 'bukti_pembayaran', file_name)
    
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="image/jpeg")  
            response['Content-Disposition'] = f'inline; filename={file_name}'
            return response
    else:
        return HttpResponse("File not found", status=404)