# app/forms.py
from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from ..models import CustomUser
from django.contrib.auth import get_user_model



class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields + ('no_telp', 'email', 'nama', 'role', 'username', 'foto')

class CustomAuthenticationForm(AuthenticationForm):
    class Meta:
        model = CustomUser
        fields = ['username', 'password']
