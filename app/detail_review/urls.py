from django.urls import path
from .views import *

urlpatterns = [
    path('<int:pk>/', lapangan_detail, name='lapangan_detail'),
    path('create-review/', create_review, name="create_review"),
    path('show-review/', show_review, name="show_review"),
]